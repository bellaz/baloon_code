//
// half turn antenna
//

FREQUENCY = 433E6; // Mhz
LIGHTSPEED = 299792458; // m/s

use <supports.scad>;
use <connection_box.scad>;
use <hollow_cylinder.scad>;

module half_turn(frequency, thickness = 3, box_size = 65, radius = 10, u_radius = 2, l_radius = 1) {
    wavelength =  1000 * LIGHTSPEED / frequency; // mm
    big_loop_height   = wavelength * 0.26;
    small_loop_height = wavelength * 0.238;
    big_loop_radius   = wavelength * 0.173 / 2;
    small_loop_radius = wavelength * 0.156 / 2;
    avg_height = (big_loop_height + small_loop_height)/2; 
    loops_h_distance = (big_loop_height - small_loop_height)/2;
    echo(avg_height);
    color("Gainsboro")
    union(){
    rotate([0, 0, 45])
    union() {
        rotate([0, 180, 0])
        connection_box_w_sma_housing(thickness, box_size, radius-thickness, thickness*3 ,         loops_h_distance, u_radius, l_radius);
    
        mirror([1,0,0])
        translate([0, 0, avg_height]) 
        connection_box(thickness, box_size, radius-thickness, thickness*3 , loops_h_distance,         u_radius, l_radius);
        
        translate([0, 0, box_size/2 - thickness/2]) 
        hollow_cylinder(h=thickness + avg_height - box_size, r=radius, thickness =          thickness);
    }
    
    translate([0, 0, loops_h_distance/2])
    union() {
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, small_loop_radius-sqrt(2)/2*box_size, l_radius*4,       l_radius*4, thickness/2, l_radius);
    
        rotate([0, 0, 180])
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, small_loop_radius-sqrt(2)/2*box_size, l_radius*4,       l_radius*4, thickness/2, l_radius);
    }
    
    translate([0, 0, -loops_h_distance/2])
    union() {
        rotate([0, 0, 270])
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, big_loop_radius-sqrt(2)/2*box_size, u_radius*4,       u_radius*4, thickness/2, u_radius);
    
        rotate([0, 0, 90])
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, big_loop_radius-sqrt(2)/2*box_size, u_radius*4,       u_radius*4, thickness/2, u_radius);
    }
    
        translate([0, 0, loops_h_distance/2 + small_loop_height])
    union() {
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, small_loop_radius-sqrt(2)/2*box_size, l_radius*4,       l_radius*4, thickness/2, l_radius);
    
        rotate([0, 0, 180])
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, small_loop_radius-sqrt(2)/2*box_size, l_radius*4,       l_radius*4, thickness/2, l_radius);
    }
    
    translate([0, 0, -loops_h_distance/2 + big_loop_height])
    union() {
        rotate([0, 0, 270])
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, big_loop_radius-sqrt(2)/2*box_size, u_radius*4,       u_radius*4, thickness/2, u_radius);
    
        rotate([0, 0, 90])
        translate([sqrt(2)/2*box_size,0,0])
        support_closed(thickness, big_loop_radius-sqrt(2)/2*box_size, u_radius*4,       u_radius*4, thickness/2, u_radius);
    }
    
    translate([0, 0, avg_height/2])
    union() {
        rotate([0, 0, 0])
        translate([radius,0,0])
        support_closed(thickness, big_loop_radius - radius, u_radius*7, u_radius*4, thickness/2, l_radius);
    
        rotate([0, 0, 180])
        translate([radius,0,0])
        support_closed(thickness, big_loop_radius - radius, u_radius*7, u_radius*4,thickness/2, u_radius);
        
    }
    
    translate([0, 0, avg_height/2])
    union() {
        rotate([0, 0, 90])
        translate([radius,0,0])
        support_closed(thickness, small_loop_radius - radius, u_radius*7, u_radius*4, thickness/2, l_radius);
    
        rotate([0, 0, 270])
        translate([radius,0,0])
        support_closed(thickness, small_loop_radius - radius, u_radius*7, u_radius*4,thickness/2, l_radius);
        
    }
    }
    
    union() {
        translate([0, 0, small_loop_height/2+loops_h_distance/2])
        linear_extrude(height = small_loop_height, center=true, convexity = 10,     twist = 180)
        translate([small_loop_radius, 0, 0])
        circle(r = l_radius);
        
        rotate([0, 0, 180])
        translate([0, 0, small_loop_height/2+loops_h_distance/2])
        linear_extrude(height = small_loop_height, center=true, convexity = 10,     twist = 180)
        translate([small_loop_radius, 0, 0])
        circle(r = l_radius);
        
        rotate([0, 0, 90])
        translate([0, 0, big_loop_height/2-loops_h_distance/2])
        linear_extrude(height = big_loop_height, center=true, convexity = 10,     twist = 180)
        translate([big_loop_radius, 0, 0])
        circle(r = l_radius);
        
        rotate([0, 0, 270])
        translate([0, 0, big_loop_height/2-loops_h_distance/2])
        linear_extrude(height = big_loop_height, center=true, convexity = 10,     twist = 180)
        translate([big_loop_radius, 0, 0])
        circle(r = u_radius);
    }
}
translate([0, 0, -50])
half_turn(FREQUENCY);
