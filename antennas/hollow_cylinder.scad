//
// hollow cylinder
//

RADIUS = 15;
HEIGHT = 30;
THICKNESS = 5;

module hollow_cylinder(h, r, thickness) {
    
    difference()  {
        cylinder(h=h,r=r);
        translate([0, 0, -h/2])
        cylinder(h=h*2,r=r-thickness);
    }
}

hollow_cylinder(HEIGHT, RADIUS, THICKNESS);