//
// Supports library
//

THICKNESS = 2.5;
LENGTH = 60;
INNER_HEIGHT = 40;
OUTER_HEIGHT = 30;
ATTACH_THICKNESS = 5;
WIRE_RADIUS = 3;

module 2d_support(length, inner_height, outer_height, attach_thickness, wire_radius) {
    difference() {
        union() {
            polygon([[-attach_thickness, 0],
                    [-attach_thickness, inner_height],
                    [length, inner_height],
                    [length, inner_height-outer_height],
                    [0,0]]);

            translate([length, inner_height-outer_height/2, 0])
            circle(outer_height/2);
        }
        translate([length, inner_height-outer_height/2, 0])
            circle(wire_radius);
    }
}

support_open(THICKNESS, LENGTH, INNER_HEIGHT, OUTER_HEIGHT, ATTACH_THICKNESS, WIRE_RADIUS);

module support_closed(thickness, length, inner_height, outer_height, attach_thickness, wire_radius) {
    translate([0, 0, -inner_height + outer_height/2])
    rotate([90, 0, 0])
    translate([0,0,-thickness/2])
    linear_extrude(thickness)
    2d_support(length, inner_height, outer_height, attach_thickness, wire_radius);
}

module support_open(thickness, length, inner_height, outer_height, attach_thickness, wire_radius) {
    translate([0, 0, -inner_height + outer_height/2])
    rotate([90, 0, 0]) 
    translate([0,0,-thickness/2])
    linear_extrude(thickness)
    difference() {
        2d_support(length, inner_height, outer_height, attach_thickness, wire_radius);
        translate([length, inner_height-outer_height/2-wire_radius, 0])
        square(outer_height);
        translate([length-wire_radius, inner_height-outer_height/2, 0])
        square(outer_height);
    }
}

support_open(THICKNESS, LENGTH, INNER_HEIGHT, OUTER_HEIGHT, ATTACH_THICKNESS, WIRE_RADIUS);

translate([0, 30, 0])
support_closed(THICKNESS, LENGTH, INNER_HEIGHT, OUTER_HEIGHT, ATTACH_THICKNESS, WIRE_RADIUS);