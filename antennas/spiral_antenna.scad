//
// spiral antenna
//

FREQUENCY = 433E6;
THICKNESS = 2;
LIGHTSPEED = 299792458;
RADIUS_WIRE_SMALL = 0.4;
RADIUS_WIRE_BIG = 2.8/2;

module spiral_antenna(frequency = FREQUENCY, thickness = THICKNESS) {
    wavelength =  1000 * LIGHTSPEED / frequency; // mm
    big_loop_height   = wavelength * 0.26;
    small_loop_height = wavelength * 0.238;
    big_loop_radius   = wavelength * 0.173 / 2;
    small_loop_radius = wavelength * 0.156 / 2;
    avg_height = (big_loop_height + small_loop_height)/2; 
    loops_h_distance = (big_loop_height - small_loop_height)/2;
    avg_radius = (big_loop_radius + small_loop_radius)/2;
    loops_r_distance = (big_loop_radius - small_loop_radius)/2;
    big_angle_wire   = atan(big_loop_height/(PI * big_loop_radius));
    small_angle_wire = atan(small_loop_height/(PI * small_loop_radius));
    echo("big angle");
    echo(big_angle_wire);
    echo("small angle");
    echo(small_angle_wire);
    band_dim = 8 * loops_r_distance;
    
    half_spiral(thickness, band_dim, avg_radius, big_loop_height);
    rotate([0, 0, 90])
    half_spiral(thickness, band_dim, avg_radius, small_loop_height);
    rotate([0, 0, 180])
    half_spiral(thickness, band_dim, avg_radius, big_loop_height);
    rotate([0, 0, 270])
    half_spiral(thickness, band_dim, avg_radius, small_loop_height);
    
    mirror([1, 0, 0])
    union() {
        half_spiral(thickness, band_dim/2, avg_radius, big_loop_height);
        rotate([0, 0, 90])
        half_spiral(thickness, band_dim/2, avg_radius, big_loop_height);
        rotate([0, 0, 180])
        half_spiral(thickness, band_dim/2, avg_radius, big_loop_height);
        rotate([0, 0, 270])
        half_spiral(thickness, band_dim/2, avg_radius, big_loop_height); 
    }
    
    translate([0, 0, -big_loop_height/2])
    difference() {
        cylinder(h=band_dim, r=avg_radius+thickness/2, center=true);
        cylinder(h=band_dim*2, r=avg_radius-thickness/2, center=true);
    }
    
    translate([0, 0, big_loop_height/2])
    difference() {
        cylinder(h=band_dim, r=avg_radius+thickness/2, center=true);
        cylinder(h=band_dim*2, r=avg_radius-thickness/2, center=true);
    }
    
    difference() {
        intersection() {
            translate([0, 0, big_loop_height/4 + avg_radius*2*sqrt(2)]){ 
                union() {
                    rotate([45, 0, 45])
                    cube(avg_radius*2*2, center=true);
                    rotate([45, 0, 135])
                    cube(avg_radius*2*2, center=true);
                }
            }
            
        difference() {
            cylinder(h=avg_radius*4, r=avg_radius+thickness/2, center=true);
            cylinder(h=avg_radius*8, r=avg_radius-thickness/2, center=true);
            }
        }
        translate([0, 0, avg_radius*2 + big_loop_height/2])
        cube(avg_radius*4, center=true);
    }
    
    translate([0, 0, - big_loop_height/2 - band_dim/2])
    cylinder(h=thickness, r=avg_radius+thickness/2);
}

module half_spiral(thickness, band_dim, radius, height) {
    
    translate([0, 0, -height/2])
    linear_extrude(height = height, convexity = 10, twist = 180)
    translate([radius-thickness/2, 0, 0])
    square([thickness, band_dim], center=true);
}

module support(thickness = 2, distance = 20, radius = 5){
    
    rotate([90, 0, 0])
    translate([0, 0, -thickness/2])
    linear_extrude(thickness)
    difference(){
    translate([0, -(distance + radius*2 + thickness*4), 0])
    polygon([[0,0], [distance + radius + thickness*2, distance + radius + thickness*2], [distance + radius + thickness*2, distance + radius*3 + thickness*6], [0, distance + radius*3 + thickness*6]]);
    translate([distance, 0, 0])
    circle(radius, center=true);
    }
}

//support();
spiral_antenna($fn =100);