//
// Connection box
//

THICKNESS = 2.5;
BOX_SIZE = 60;
BOX_TOPDOWN_RADIUS = 7;
TRAVERSE_HOLES_H_DISTANCE = 25;
TRAVERSE_HOLES_PAIR_DISTANCE = 12.5;
TRAVERSE_HOLES_UPPER_RADIUS = 4;
TRAVERSE_HOLES_LOWER_RADIUS = 2;

CONNECTOR_INNER_RADIUS = 6;
CONNECTOR_SCREW_RADIUS = 3;
CONNECTOR_SCREW_DISTANCE = 15;

module connection_box(thickness, box_size, box_topdown_radius, traverse_holes_pair_distance, traverse_holes_h_distance, traverse_holes_upper_radius, traverse_holes_lower_radius)
union() {
    // box code
    difference(){
        cube(box_size, true);
        translate([thickness, 0, 0])
        cube([box_size, box_size-thickness*2,box_size-thickness*2], true);
        cylinder(h=box_size*2, r=box_topdown_radius, center=true);
        translate([0, 0, traverse_holes_h_distance/2])
        rotate([90, 0, 45])
        union(){
            translate([traverse_holes_pair_distance/2, 0, 0])
            cylinder(h=box_size*2, r=traverse_holes_upper_radius, center=true);

            translate([-traverse_holes_pair_distance/2, 0, 0])
            cylinder(h=box_size*2, r=traverse_holes_upper_radius, center=true);
        }

        translate([0, 0, -traverse_holes_h_distance/2])
        rotate([90, 0, -45])
        union(){
            translate([traverse_holes_pair_distance/2, 0, 0])
            cylinder(h=box_size*2, r=traverse_holes_lower_radius, center=true);

            translate([-traverse_holes_pair_distance/2, 0, 0])
            cylinder(h=box_size*2, r=traverse_holes_lower_radius, center=true);
        }
    }
    // holder code
    translate([0, 0, box_size/2])
    rotate([0, 90, 0])
    translate([0,0, -thickness/2])
    linear_extrude(thickness)
    difference(){
        circle(box_topdown_radius*2, true);
        circle(box_topdown_radius, true);
        translate([box_topdown_radius+thickness/2,0, 0])
        square([box_topdown_radius*2, box_topdown_radius*4], true);
    }
}

module connection_box_w_sma_housing(thickness, box_size, box_topdown_radius, traverse_holes_pair_distance, traverse_holes_h_distance, traverse_holes_upper_radius, traverse_holes_lower_radius) {
    
    difference() {
      connection_box(thickness, box_size, box_topdown_radius,                         traverse_holes_pair_distance, traverse_holes_h_distance, traverse_holes_upper_radius, traverse_holes_lower_radius);
    
    rotate([0, -90, 0])
    rotate([0, 0, 45])
    union() {
        cylinder(h=box_size, r=CONNECTOR_INNER_RADIUS);
        translate([CONNECTOR_SCREW_DISTANCE, 0, 0])
        cylinder(h=box_size, r=CONNECTOR_SCREW_RADIUS);
        translate([-CONNECTOR_SCREW_DISTANCE, 0, 0])
        cylinder(h=box_size, r=CONNECTOR_SCREW_RADIUS);
        translate([0, CONNECTOR_SCREW_DISTANCE, 0])
        cylinder(h=box_size, r=CONNECTOR_SCREW_RADIUS);
        translate([0, -CONNECTOR_SCREW_DISTANCE, 0])
        cylinder(h=box_size, r=CONNECTOR_SCREW_RADIUS);
    }        
    }  
}

translate([0, -BOX_SIZE, 0])
connection_box(THICKNESS, BOX_SIZE, BOX_TOPDOWN_RADIUS, TRAVERSE_HOLES_PAIR_DISTANCE, TRAVERSE_HOLES_H_DISTANCE, TRAVERSE_HOLES_UPPER_RADIUS, TRAVERSE_HOLES_LOWER_RADIUS);


translate([0, BOX_SIZE, 0])
connection_box_w_sma_housing(THICKNESS, BOX_SIZE, BOX_TOPDOWN_RADIUS, TRAVERSE_HOLES_PAIR_DISTANCE, TRAVERSE_HOLES_H_DISTANCE, TRAVERSE_HOLES_UPPER_RADIUS, TRAVERSE_HOLES_LOWER_RADIUS);



