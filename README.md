# MSK Baloon project code #

This repo contains the code to run the raspberry pi0 controller that equips the 
stratosferic project and the ground receiver station.

---
## Problems ##

* Track the baloon path even when it is outside the GSM/GPRS signal.
* Make a backup of the primary recording device (GoPRo).
* Measure altitude, pressure, magnetic field, acceleration and internal/external temperature.
* Measure battery/system status
* Send the information back to a ground station.
* Make a reliable implementation of the above points.

## Concept ##

A Processing Unit (PU) has the duty to perform all the required operations to read
the various sensors and store/send them through the output devices.
A camera records the outside environment. Because the available wireless solutions are 
not able to transfer a real-time data stream to the ground the camera output video
is recorded on a solid state memory (SD).
All the other informations should be send through the radio apparatus. A certain amount
of redundancy is foreseen (eg. double antenna).

---
## Equipment ##
Here are listed the equipment of Probe and Ground section
### Probe section ###
* 1x[raspberry pi0 (CPU)](https://www.raspberrypi.org/products/raspberry-pi-zero/)
* 1x[160(deg) CSI camera](https://shop.pimoroni.com/products/raspberry-pi-zero-camera-module?variant=3031238213642)
* 2x[433MHz LoRa Semtech SX1278 boards spi transceiver](https://cdn-shop.adafruit.com/product-files/3179/sx1276_77_78_79.pdf)
* 2x[Digital Barometric Pressure Sensor I2C BMP280 MPU9250 3.3V AIP](https://artofcircuits.com/product/10dof-gy-91-4-in-1-mpu-9250-and-bmp280-multi-sensor-module)
* 2x[MCP9808 I2C IIC High Accuracy Temperature Sensor Breakout Module](https://www.adafruit.com/product/1782)
* 2x[GY-NEO6MV2 GPS trackers RS232](https://www.openimpulse.com/blog/products-page/product-category/gy-neo6mv2-gps-module/)
* 1x[Real Time Clock I2C RTC DS1307 Zeit Uhr Module AT24C32](https://tutorials-raspberrypi.de/raspberry-pi-rtc-modul-i2c-echtzeituhr/)
* 1xGPS mini antenna
* 1xGPS big antenna
* 1xStep down switching regulator to 5V
* 2xLithium 9V batteries
* 1x(to remove prior the launch)[PL2303HX usb-RS232 converter](http://www.prolific.com.tw/UserFiles/files/ds_pl2303HXD_v1_4_4.pdf)
* Eventual signaling leds?

### Ground section ###

* 1x[LoRa32 433MHz (radio+ground CPU)](https://hackaday.io/project/26991-esp32-board-wifi-lora-32)
* 1xPatch cable u.fl to sma female 
* 1x12dBi Half Dipole antenna

---
## Probe section bus/register/pin assignment ##

### I2C Addresses ###

[I2C on raspberry](https://diyprojects.io/activate-i2c-bus-raspberry-pi-3-zero/)

* BMP280-1  : 0x76 #[I2C-1](http://ozzmaker.com/i2c/)
* BMP280-2  : 0x77 #[I2C-1](http://ozzmaker.com/i2c/)
* MPU9250-1 : 0x68 #[I2C-1](http://ozzmaker.com/i2c/)
    * AK8963 : 0x0C <https://www.akm.com/akm/en/file/datasheet/AK8963C.pdf>
* MPU9250-2 : 0x69 #[I2C-1](http://ozzmaker.com/i2c/)
    * AK8963 : 0x0C <https://www.akm.com/akm/en/file/datasheet/AK8963C.pdf>
* MCP9808-1 : 0x18 #[I2C-1](http://ozzmaker.com/i2c/)
* MCP9808-2 : 0x19 #[I2C-1](http://ozzmaker.com/i2c/)
* DS1307    : 0x68 #[I2C-0](https://www.raspberrypi.org/forums/viewtopic.php?t=144719)
* ADS1115   : 0x48 #[I2C-1](http://ozzmaker.com/i2c/)

### Change I2C Address ###

* BMP280  : Pin 6 (SDO)
* MPU9250 : Pin 6 (SDO)
* MCP9808 : Pin 3 (AD0), Pin 1,2 -> 0

### Additional informations ###
* CSI camera
    1. <https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspivid.md>
    2. <https://www.raspberrypi.org/documentation/raspbian/applications/camera.md>
* BMP280
    1. <https://github.com/ControlEverythingCommunity/BMP280/blob/master/Python/BMP280.py>
    2. <https://artofcircuits.com/product/10dof-gy-91-4-in-1-mpu-9250-and-bmp280-multi-sensor-module>
* MPU9250
    1. <https://www.invensense.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf>
    2. <https://artofcircuits.com/product/10dof-gy-91-4-in-1-mpu-9250-and-bmp280-multi-sensor-module>
    3. <https://github.com/MomsFriendlyRobotCompany/mpu9250/blob/master/mpu9250/mpu9250.py>
* AK8963 
    1. <https://www.akm.com/akm/en/file/datasheet/AK8963C.pdf>
    2. <https://github.com/MomsFriendlyRobotCompany/mpu9250/blob/master/mpu9250/mpu9250.py>
* MCP9808
    1. <https://www.adafruit.com/product/1782>
    2. <https://www.hackster.io/Dcube/temperature-monitoring-using-mcp9808-and-raspberry-pi-962fd2>
    3. <https://github.com/ControlEverythingCommunity/MCP9808/blob/master/Python/MCP9808.py>
* DS1307 
    1. <https://www.raspberrypi.org/forums/viewtopic.php?t=213312>
    2. <https://tutorials-raspberrypi.de/raspberry-pi-rtc-modul-i2c-echtzeituhr/>
    3. <https://thepihut.com/blogs/raspberry-pi-tutorials/17209332-adding-a-real-time-clock-to-your-raspberry-pi>
* ADS1115
    1. <https://learn.adafruit.com/adafruit-4-channel-adc-breakouts/python-circuitpython>
    2. <https://github.com/ControlEverythingCommunity/ADS1115/blob/master/Python/ADS1115.py>
* GY-NEO6MV2
    1. <https://circuitdigest.com/microcontroller-projects/raspberry-pi-3-gps-module-interfacing>
    2. <http://abyz.me.uk/rpi/pigpio/>
* LoRa Semtech SX1278
    1. <https://pypi.org/project/pyLoRa/>
    2. <https://github.com/rpsreal/pySX127x/blob/master/SX127x/LoRa.py>
* PL2303HX
    1. <https://raspberrypi.stackexchange.com/questions/9991/connecting-a-usb-to-serial-ttl-pl2303hx-to-raspberry-pi>
* LORA32
    1. https://robotzero.one/heltec-wifi-lora-32/

### Python libraries (RPi0) ###

* Control GPIO pins    : [RPi.GPIO](https://pypi.org/project/RPi.GPIO/)
* Control I2C bus      : [smbus](https://pypi.org/project/smbus-cffi/)
* Control spi bus      : [spidev](https://pypi.org/project/spidev/)
* Software RS232       : [pigpio](http://abyz.me.uk/rpi/pigpio/)
* SX1278               : [pylora](https://pypi.org/project/pyLoRa/)
* GPS decoding library : [pynmea](https://pypi.org/project/pynmea/)
* Hashing              : [hashlib](https://docs.python.org/3/library/hashlib.html)
* Cryptography         : [pycrypto](https://pypi.org/project/pycrypto/)
    * [Example1](https://www.laurentluce.com/posts/python-and-cryptography-with-pycrypto/)
    * [Example2(AES simple)](https://pycryptodome.readthedocs.io/en/latest/src/cipher/aes.html)
### LORA32 related ###

* Set up <https://robotzero.one/heltec-wifi-lora-32/>
* Arduino-esp32 <https://github.com/espressif/arduino-esp32>

---

### Pin mapping (RPi0) ###

![Raspberry pi 0 mapping](./images/raspberry-pi-pinout.png)

I2C-0:

* SDA  : 0
* SCL  : 1

I2C-1:

* SDA  : 2
* SCL  : 3

SPI:

* MISO : 9
* MOSI : 10
* CLK  : 11
* CE0  : 8
* CE1  : 7

RS232-1:

* TX   : 22
* RX   : 23

RS232-2:

* TX   : 24
* RX   : 25

RS232-native(PL2303HX):

* TX   : 14
* RX   : 15

--- 

### Device Images ###
RPi0:

![RPi0](./images/RPi0.jpg)

CSI camera:

![CSI_camera](./images/CSI_camera.jpg)

GY-91:

![GY-91](./images/GY-91.jpg)

SX1278:

![SX1278](./images/SX1278.jpg)

GY-NEOMV2 small antenna:

![GY-NEO6MV2](./images/GY-NEO6MV2.jpg)

GY-NEOMV2 big antenna:

![GY-NEO6MV2_big](./images/GY-NEO6MV2_big.jpg)

DS1307:

![DS1307](./images/DS1307.jpg)

ADS1115:

![ADS1115](./images/ADS1115.jpg)

DC-DC converter:

![DC_converter](./images/DC_converter.jpg) 

PL2303HX:

![PL2303HX.jpg](./images/PL2303HX.jpg) 

LORA32:

![LORA32.jpg](./images/LORA32.jpg)

12dbi antenna:

![12dbi_antenna](./images/12dbi_antenna.jpg)              

RF cable:

![RF_cable](./images/RF_cable.jpg)

