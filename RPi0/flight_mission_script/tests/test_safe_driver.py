import unittest
from white_angel.config import ATTEMPTS_BLOCK,RECONNECT_ATTEMPTS 
from white_angel.safe_driver import SafeDriver

class SafeDriverExampleConnect(SafeDriver):
    '''
        Fake version of Safe driver
    '''
    def __init__(self, prefix, number_failures):
        super(SafeDriverExampleConnect, self).__init__(prefix)
        self._number_failures = number_failures

    def connect(self):
        if self._number_failures > 0:
            self._number_failures -= 1
            raise Exception
        return True

    def transact(self, driver_hndl, *_):
        if driver_hndl != True:
            raise Exception
        return {'read' : 'happiest'} 

class TestSafeDriver(unittest.TestCase):

    def test_working(self):
        driver = SafeDriverExampleConnect('example', 0)
        for i in range((ATTEMPTS_BLOCK + RECONNECT_ATTEMPTS)*10):
            result = driver.execute()
            self.assertEqual(True, result['example_online'])
            self.assertEqual(0, driver.attempts_block_n)
            self.assertEqual(0, driver.reconnect_attempts_n)
            self.assertEqual('happiest', result['example_read'])

    def test_not_working(self):
        driver = SafeDriverExampleConnect('example', ATTEMPTS_BLOCK + RECONNECT_ATTEMPTS)
        for i in range((ATTEMPTS_BLOCK + RECONNECT_ATTEMPTS)*10):
            result = driver.execute()
            self.assertEqual(False, result['example_online'])
            if i+1 < RECONNECT_ATTEMPTS:
                self.assertEqual(i+1, driver.reconnect_attempts_n)
            else:
                self.assertEqual(RECONNECT_ATTEMPTS, driver.reconnect_attempts_n)
                
    def test_working_before_stop(self):
        pass

    def test_after_stop(self):
        pass

if __name__ == '__main__':
    unittest.main()
