'''
    Base abstract driver class that wraps unsafe code of a device trying to reconnect
    it if it fails/crash
'''
from abc import ABC, abstractmethod
from .config import ATTEMPTS_BLOCK, RECONNECT_ATTEMPTS

class SafeDriver(ABC):
    '''
        Template safe driver that allows to run unsafe code without crashing the main thread
        It returns a dictionary of the returned values when executed with run
    '''

    def __init__(self, prefix):
        '''
            initialize the class with the number of blocking attempts nr and
            reconnect attempts  nr = 0

            prefix is added to the keys of the returned data array

            ONLY SAFE CODE HERE
        '''
        self._prefix = prefix
        self.attempts_block_limit = ATTEMPTS_BLOCK
        self.reconnect_attempts_limit = RECONNECT_ATTEMPTS
        self.attempts_block_n = 0
        self.reconnect_attempts_n = 0
        self._driver_hndl = None #contains the driver connection informations
        self.last_exception = None
        self._online = False
        super(SafeDriver, self).__init__()

    @abstractmethod
    def connect(self):
        '''
            this function has to be redefined to include the device initialization
            the handle of the driver has to be returned at the end of it
            If it crashes the driver retry to execute it every time 'execute' is called

            AN HANDLE THAT DIFFERS THAN NONE HAS TO BE RETURNED ALWAYS
        '''
        pass

    @abstractmethod
    def transact(self, driver_hndl, *passed_params):
        '''
            this function perform a data IO transactions on the device.
            if it crashes, driver_hndl is scrapped and 'connect' is executed again.
            returns the read data THAT MUST BE A DICTIONARY
        '''
        pass

    def execute(self, *passed_parameters):
        '''
            this function tries to run 'transact'.
            it calls 'connect' at first if a driver handle is missing(None),
            If one of these function fails it sets handle = None
            After failing reconnect_attempts_times times it ignores the calls to 'execute'
            for attempts_block_limit times
        '''
        if self.reconnect_attempts_n == self.reconnect_attempts_limit:
            self.attempts_block_n += 1
            if self.attempts_block_n == self.attempts_block_limit:
                self.attempts_block_n = 0

        if ((self.reconnect_attempts_n < self.reconnect_attempts_limit) or
                (self.attempts_block_n == 0)):

            try:
                if self._driver_hndl is None:
                    self._driver_hndl = self.connect()
                returned_data = self.transact(self._driver_hndl, passed_parameters)
                if not isinstance(returned_data, dict):
                    raise TypeError 
                self._online = True
                returned_data = self.decorate_returned_data(returned_data)
                self.attempts_block_n = 0
                self.reconnect_attempts_n = 0
                return returned_data
            except Exception as exception:
                self._driver_hndl = None
                self._online = False
                if self.reconnect_attempts_n < self.reconnect_attempts_limit:
                    self.reconnect_attempts_n += 1
                self.reconnect_attempts_n = self.reconnect_attempts_n
                self.attempts_block_n = self.attempts_block_n
                self.last_exception = exception
        returned_data = {}
        returned_data = self.decorate_returned_data(returned_data)
        return returned_data

    def decorate_returned_data(self, returned_data):
        '''
            decorate returned data with additional informations
        '''
        returned_data['online'] = self._online
        returned_data = dict((self._prefix + '_' + key, value) for
                             (key, value) in
                             returned_data.items())
        return returned_data
