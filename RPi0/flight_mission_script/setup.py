#!/usr/bin/env python3
'''
    Install script for the white angel
'''

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    '''
        read a file (for docs)
    '''
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="white_angel",
    version="0.0.1",
    author="Andrea Bellandi",
    author_email="bellaz89@gmail.com",
    description=("flight navigation scripts for the baloon project"),
    license="GPL",
    keywords="baloon flight radio",
    packages=['white_angel', 'tests'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Communications :: Ham Radio",
        "Topic :: Scientific/Engineering :: Atmospheric Science",
        "Topic :: Scientific/Engineering :: Astronomy",
        "Programming Language :: Python :: 3 :: Only"
    ],
    test_suite="tests",
)

